package org.launchcode.dataCleaner;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class DataCleanerApplication {

	public DataCleanerApplication() {
	}

	static ClassLoader classLoader = new DataCleanerApplication().getClass().getClassLoader();



	public static List<List<String>> loadData(String sourcePath) throws IOException {
		List<List<String>> records = new ArrayList<List<String>>();
		try (CSVReader csvReader = new CSVReader(new FileReader(sourcePath));) {
			String[] values = null;
			while ((values = csvReader.readNext()) != null) {
				List<String> target = new ArrayList<String>(Arrays.asList(values));
				records.add(target);
			}
		}
		return records;
	}


	public static List<List<String>> cleanData(List<List<String>> records) throws IOException {
		//load world cities for cityparse
		List<List<String>> worldData = loadData("src/main/resources/simplemaps_worldcities_basicv1.5/worldcities.csv");

		List<List<String>> lines = new ArrayList<>();
		for (List<String> record : records) {
			int size = record.size();
			if (size < 10) {
				if (record.contains("report_date")) {
					record.add("location_geometry");
				} else {
					String country = record.get(1);
					if (country.equals("El_Salvador")) {
						record.add("POINT(-88.889675 13.7942)");
					} else if (country.equals("Nicaragua")) {
						record.add("POINT(-85.2072 12.8654)");
					} else {
						String city = country.split("-")[1];
						country = country.split("-")[0].replaceAll("_", " ");
						for (List<String> target : worldData) {
							if (target.contains(city) && target.contains(country)) {
								String lat = target.get(2);
								String lon = target.get(3);
								record.add("POINT(" + lon + " " + lat + ")");
							}
						}
					}
				}
			}

		}
		return records;
	}

	public static void saveData(String filePath, List<List<String>> lines) throws IOException {
		// first create file object for file placed at location
		// specified by filepath
		File file = new File(filePath);

		try {
			// create FileWriter object with file as parameter
			FileWriter outputFile = new FileWriter(file);

			// create CSVWriter object filewriter object as parameter
			CSVWriter writer = new CSVWriter(outputFile);

			// create a List which contains String array
			List<String[]> data = new ArrayList<String[]>();
			for (List<String> line : lines) {
				String[] target = line.toArray(new String[0]);
				data.add(target);
			}
			writer.writeAll(data);

			// closing writer connection
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public static void main(String[] args) throws IOException {
		SpringApplication.run(DataCleanerApplication.class, args);


		List<List<String>> targetSet = loadData("src/main/resources/zika-data/El_Salvador-2016-02-20.csv");
		List<List<String>> cleanSet = cleanData(targetSet);
		saveData("src/main/resources/zika-data/New_El_Salvador-2016-02-20.csv", cleanSet);
		List<List<String>> targetSet1 = loadData("src/main/resources/zika-data/Haiti_Zika-2016-02-03.csv");
		List<List<String>> cleanSet1 = cleanData(targetSet1);
		saveData("src/main/resources/zika-data/New_Haiti_Zika-2016-02-03.csv", cleanSet1);
		List<List<String>> targetSet2 = loadData("src/main/resources/zika-data/Mexico_Zika-2016-02-20.csv");
		List<List<String>> cleanSet2 = cleanData(targetSet2);
		saveData("src/main/resources/zika-data/New_Mexico_Zika-2016-02-20.csv", cleanSet2);
		List<List<String>> targetSet3 = loadData("src/main/resources/zika-data/MINSA_ZIKA_Search-week_08.csv");
		List<List<String>> cleanSet3 = cleanData(targetSet3);
		saveData("src/main/resources/zika-data/New_MINSA_ZIKA_Search-week_08.csv", cleanSet3);
		List<List<String>> targetSet4 = loadData("src/main/resources/zika-data/Panama_Zika-2016-02-18.csv");
		List<List<String>> cleanSet4 = cleanData(targetSet4);
		saveData("src/main/resources/zika-data/New_Panama_Zika-2016-02-18.csv", cleanSet4);
	}
}